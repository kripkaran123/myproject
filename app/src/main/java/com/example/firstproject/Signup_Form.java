package com.example.firstproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toolbar;

public class Signup_Form extends AppCompatActivity {
    private EditText Fullname;
    private EditText Username;
    private EditText Email;
    private EditText Password1;
    private EditText Password2;
    private RadioGroup radio;
    private Button submitBtn;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__form);
        android.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fullname = findViewById(R.id.Fullname);
        Username = findViewById(R.id.Username);
        Email = findViewById(R.id.Email);
        Password1 = findViewById(R.id.password1);
        Password2 = findViewById(R.id.password2);

        submitBtn = findViewById(R.id.submitBtn);
    }
    private void setSupportActionBar(android.widget.Toolbar toolbar) {
        toolbar= findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login System");

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        int id = item.getItemId();
        if (id ==R.id.exit) {
            onBackPressed();
            return true;

        } else if (id ==R.id.search) {

        }


        return false;
    }



//        @Override
//        protected void onCreate (Bundle savedInstanceState){
//            super.onCreate(savedInstanceState);
//            setContentView(R.layout.activity_signup__form);
//            getSupportActionBar().setTitle("Signup Form");
//        }
}

